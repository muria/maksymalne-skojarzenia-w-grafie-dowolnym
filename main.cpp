#include <iostream>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <utility>
#include <vector>

using namespace std;

class Cycle{
private:
	map<int,set<int> > nodes;
	set<pair<int,int> > M;
public:
	void add(int a, int b){
		nodes[a].insert(b);
	}
	void addPair(pair<int,int> p){
		M.insert(p);
	}
	void addPair(int a, int b){
		M.insert(pair<int,int>(a,b));
	}
	set<pair<int,int> > pairs(){ return M;}
	
	friend ostream& operator<<(ostream& os, const Cycle& c){
		for(auto i : c.nodes){
			os << i.first << " ";
		}
		os << ": M: ";
		for(auto i : c.M){
			os << "(" <<i.first<<","<<i.second<<") ";
		}
		return os;
	}
	
	map<int,set<int> > getNodes(){ return nodes; }
	
	list<int> getExtPath(int ne){
		int node;
		for(auto i : nodes){
			for(auto j : nodes[i.first]){
				if(j == ne){
					node = i.first;
					goto break1;
				}
			}
		}
		break1:
		for(auto p : M){
			if(p.first == node || p.second == node){
				int a = (p.first == node)?p.second:p.first;
				map<int,bool> vis;
				map<int,int> path;
				vis[node] = true;
				path[node] = -1;
				vis[a] = true;
				path[a] = node;
				queue<int> Q;
				Q.push(a);
				while(!Q.empty()){
					int n = Q.front(); Q.pop();
					for(int i : nodes[n]){
						if(!vis[i]){
							vis[i] = true;
							path[i] = n;
							Q.push(i);
						}
						else if(i != path[n]){
							list<int>extPath;
							while(n!=-1){
								extPath.push_front(n);
								n = path[n];
							}
							return extPath;
						}
					}
				}
			}
		}
		return list<int>();
	};
};

class Graph{
private:
	map<int, set<int> > nodes;
	map<int,int> components;
	map<int,Cycle> cycles;
	bool keepComponents;
	int cycNum = -2;
	
	//Zwraca x,y takie, że x zawiera się w L i sasiaduje z y ktore nie zawiera sie w L
	//Lub (-1,-1) jeśli takie x,y nie istnieją
	bool findXinL(Graph *L, int *x, int *y){
		for(auto i: L->nodes){
			if( L->nodes[i.first].size() < 2){
				for(int node : nodes[i.first]){
					if( L->nodes.find(node) == L->nodes.end() ){
						*x = i.first;
						*y = node;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	void getFreeNodes(Graph *L, set<int> *freeNodes, set<pair<int,int> > *M){
		L->clear();
		freeNodes->clear();
		set<int> nodesInM;
		for(auto m: *M){
			nodesInM.insert(m.first);
			nodesInM.insert(m.second);
		}
		for(auto node : nodes){
			if(nodesInM.find(node.first) == nodesInM.end()){
				L->nodes[node.first] = set<int>();
				if(L->keepComponents){
					L->components[node.first] = node.first;
				}
				freeNodes->insert(node.first);
			}
		}
	}
	
	bool findx1x2(Graph *L, int *x, int *y){
		for(pair<int, set<int> > a: L->nodes){
			//Jeśli wierzchołek jest zewnętrzny
			if(a.second.size()<2){
				//Sprawdzamy jego sąsiadów w oryginalnym grafie
				for(int b:nodes[a.first]){
					auto bList = L->nodes.find(b);
					//Sąsiad istnieje w L i jest zewnętrzny
					if(bList!=L->nodes.end() && bList->second.size() < 2){
						*x = a.first;
						*y = b;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	int findZ(set<pair<int,int> > M, int y){
		for(auto i : M){
			if(y == i.first) return i.second;
			if(y == i.second) return i.first;
		}
		return -1;
	}
	
	list<int> getCycle(Graph *L, int x, int y){
		list<int> C;
		queue<int> Q;
		map<int,bool> vis; //Mapa odwiedzeń
		map<int,int> path; //Mapa ścieżek
		vis[x] = true;
		path[x] = -1;
		//Wyszukujemy ścieżkę, ale z pominięciem bezpośredniej drogi z x do y (co nie jest potrzebne bo x,y nie mają bezpośredniego połączenia w L)
		for(int i : L->nodes[x]){
			if(i!=y){
				Q.push(i);
				path[i] = x;
			}
		}
		while(!Q.empty()){
			int a = Q.front(); Q.pop();
			vis[a] = true;
			for(int b : L->nodes[a]){
				if(!vis[b]){
					path[b] = a;
					vis[b] = true;
					Q.push(b);
					if(b == y){
						while(b!=-1){
							C.push_front(b);
							b = path[b];
						}
						return C;
					}
				}
			}
		}
		return C;
	}
	
	list<int> getPathToC(Graph *L, set<int> *fN, list<int> *C){
		set<int> inC;
		for(int i:*C){
			inC.insert(i);
		}
		for(int external : *fN){
			queue<int> Q;
			map<int,bool> vis;
			map<int,int> path;
			Q.push(external);
			vis[external] = true;
			path[external] = -1;
			while(!Q.empty()){
				int a = Q.front(); Q.pop();
				if( inC.find(a) != inC.end() ){
					list<int>pathToC;
					while(a!=-1){
						pathToC.push_front(a);
						a = path[a];
					}
					return pathToC;
				}
				for(int b : L->nodes[a]){
					if(!vis[b]){
						path[b] = a;
						vis[b] = true;
						Q.push(b);
					}
				}
			}
		}
		return list<int>();
	}
	
	list<int> getPathToX(Graph *L, set<int> *fN, int x){
		queue<int> Q;
		map<int,bool> vis;
		map<int,int> path;
		Q.push(x);
		vis[x] = true;
		path[x] = -1;
		while(!Q.empty()){
			int a = Q.front(); Q.pop();
			if(fN->find(a) != fN->end()){
				list<int> pathToX;
				while(a!=-1){
					pathToX.push_back(a);
					a = path[a];
				}
				return pathToX;
			}
			for(auto b : L->nodes[a]){
				if( vis[b] ) continue;
				vis[b] = true;
				path[b] = a;
				Q.push(b);
			}
		}
		return list<int>();
	}
	
	void swapM(set<pair<int,int> > *M, list<int> *P){
		int a =-1;
		for(int b : *P){
			if(a==-1){
				a = b;
				continue;
			}
			set<pair<int,int> >::iterator it;
			if( (it=M->find(pair<int,int>(a,b))) != M->end() ){
				M->erase(it);
			}
			else if( (it=M->find(pair<int,int>(b,a))) != M->end() ){
				M->erase(it);
			}
			else{
				M->insert(pair<int,int>(a,b));
			}
			a = b;
		}
	}
	
	void removeNode(int node){
		for(int i : nodes[node]){
			nodes[i].erase(node);
		}
		nodes.erase(node);
	}
	
	void mergeCycle(list<int> *C, set<pair<int,int> > *M, Graph *L, set<int> *freeNodes){
		cycles[cycNum] = Cycle();
		set<int> inC;
		for(int i : *C){ inC.insert(i); }
		map<int,set<int> > oldL = L->nodes;
		int pairs=0;
		//Dodajemy te z par, które są częścią cyklu
		for(auto p : *M){
			if(inC.find(p.first) != inC.end() || inC.find(p.second) != inC.end() ){
				cycles[cycNum].addPair(p);
			}
		}
		for(auto p : cycles[cycNum].pairs()){
			M->erase(p);
			//Co najmniej jeden z wierzchołów musi zawierać się w C. Ten który sie nie zawiera tworzy nową parę z cycNum
			if(inC.find(p.first)==inC.end() ){
				M->insert(pair<int,int>(cycNum, p.second));
				pairs++;
			}
			else if(inC.find(p.second)==inC.end()){
				M->insert(pair<int,int>(p.first, cycNum));
				pairs++;
			}
		}
		L->nodes[cycNum] = set<int>();
		L->components[cycNum] = cycNum;
		for(int cnode : *C){
			auto clone = nodes[cnode];
			for(int node : clone){
				cycles[cycNum].add(cnode,node);
				//Wierzchołek nie zawiera się w Cyklu
				if(inC.find(node) == inC.end()){
					nodes[node].erase(cnode);
					nodes[node].insert(cycNum);
					nodes[cycNum].insert(node);
				}
				nodes.erase(cnode);
			}
			for(int node : oldL[cnode]){
				if(inC.find(node) == inC.end()){
					L->add(node, cycNum);
				}
				L->nodes[node].erase(cnode);
			}
			L->nodes.erase(cnode);
		}
		if(pairs==0){
			freeNodes->insert(cycNum);
		}
		
		cycNum--;
	}
	
		void unfoldAllCycles(set<pair<int,int> > *M, list<int> *P1=nullptr){
		while(++cycNum <= -2){
			for(auto node : nodes[cycNum]){
				nodes[node].erase(cycNum);
			}
			nodes.erase(cycNum);
			for(auto p : *M){
				if(p.first == cycNum || p.second == cycNum){
					M->erase(p);
				}
			}
			for(auto cnode : cycles[cycNum].getNodes()){
				for(int node : cnode.second){
					add(cnode.first, node);
				}
			}
			for(auto p : cycles[cycNum].pairs()){
				M->insert(p);
			}
			
			if(P1 != nullptr){

				if(P1->front() == cycNum){
					P1->pop_front();
					list<int> extPath = cycles[cycNum].getExtPath(P1->front());
					P1->insert(P1->begin(), extPath.rbegin(), extPath.rend());
				}
				else if(P1->back() == cycNum){
					P1->pop_back();
					list<int> extPath = cycles[cycNum].getExtPath(P1->back());
					P1->insert(P1->end(), extPath.begin(), extPath.end());
				}
			}
			cycles.erase(cycNum);
		}
		cycNum = -2;
	}
	
	void print(Graph *L, set<pair<int,int> > *M, set<int> *freeNodes){
		cout << "Graph:\n";
		for(auto i : nodes){
			cout << i.first << ": ";
			for(auto j : i.second){
				cout << j << " ";
			}
			cout << endl;
		}
		cout << "\nM:\n";
		for(auto a : *M){
			cout << a.first << " – " << a.second << endl; 
		}
		cout << "\nFree:\n";
		for(auto a : *freeNodes){
			cout << a << " ";
		}
		cout << "\n\nL:\n";
		for(auto node : L->nodes){
			cout << node.first << ": ";
			for(int i : node.second){
				cout << i << " ";
			}
			cout << endl;
		}
		cout << "\nC:\n";
		for(auto cycle : cycles){
			cout << cycle.first << ": " << cycle.second << endl;
		}

	}
	
public:
	Graph(bool rc=false){
		keepComponents=rc;
	}
	inline int size(){	return nodes.size(); }
	
	void add(int a, int b){
		map<int,set<int> >::iterator it;
		if( (it=nodes.find(a)) == nodes.end() ){
			it = (nodes.insert(pair<int,set<int> >(a, set<int>()))).first;
			if(keepComponents){
				components.insert(pair<int,int>(a,a));
			}
		}
		it->second.insert(b);
		if( (it=nodes.find(b)) == nodes.end() ){
			it = (nodes.insert(pair<int,set<int> >(b, set<int>()))).first;
			if(keepComponents){
				components.insert(pair<int,int>(b,b));
			}
		}
		it->second.insert(a);
		if(keepComponents){
			int c=min(components[a],components[b]);
			queue<int> Q;
			if(components[a] != components[b]){
				Q.push( (components[a]<components[b])?b:a );
				components[Q.front()] = c;
			}
			while(!Q.empty()){
				int n = Q.front(); Q.pop();
				for(auto i: nodes[n]){
					if(components[i]!=c){
						components[i] = c;
						Q.push(i);
					}
				}
			}			
		}
	}
	
	void clear(){
		nodes.clear();
		components.clear();
	}
	
	bool isSameComponent(int a, int b){
		return components[a]==components[b];
	}
	
	set<pair<int,int> > edmonds(){
		set<pair<int,int> > M;
		Graph L(true);
		set<int> freeNodes;
		
		getFreeNodes(&L, &freeNodes, &M);
		
		do{			
			int x, y;
			if( findXinL(&L,&x,&y) ){
				int z = findZ(M, y);
				L.add(x, y);
				L.add(y, z);
			}
			else if( findx1x2(&L,&x,&y) ){
				if(L.isSameComponent(x,y)){
					list<int> C = getCycle(&L, x, y);
					list<int> P = getPathToC(&L, &freeNodes, &C);
					swapM(&M, &P);
					mergeCycle(&C, &M, &L, &freeNodes);
				}
				else{
					list<int> P1 = getPathToX(&L, &freeNodes, x);
					list<int> P2 = getPathToX(&L, &freeNodes, y);
					P1.insert(P1.end(), P2.rbegin(), P2.rend());
					unfoldAllCycles(&M, &P1);
					swapM(&M, &P1);
					getFreeNodes(&L, &freeNodes, &M);
				}
			}
			else{
				unfoldAllCycles(&M);
				return M;
			}
				
		}while(true);
	};
	
	void addTestData(){
		add(0,1);
		add(0,7);
		add(0,3);
		add(1,2);
		add(1,3);
		add(1,4);
		add(2,4);
		add(2,6);
		add(3,7);
		add(3,8);
		add(4,5);
		add(4,6);
		add(5,8);
		add(6,9);
		add(7,8);
		add(8,9);
	}
	
};

int main(int argc, char **argv) {
    int a, b;
	Graph graph;
	graph.addTestData();
	/*
	while(cin >> a >> b){
		graph.add(a, b);
	}
	*/
	auto M = graph.edmonds();
	for(auto i : M){
		cout << "(" << i.first << ", " << i.second << ")\n";
	}
    return 0;
}
